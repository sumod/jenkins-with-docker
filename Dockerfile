﻿FROM jenkins/jenkins:lts-alpine

ENV VERSION "17.12.1-ce"

# Gets the specified linux static build, ref https://github.com/nathanielc/docker-client
RUN curl -L -o /tmp/docker-$VERSION.tgz https://download.docker.com/linux/static/stable/x86_64/docker-$VERSION.tgz
RUN tar -xz -C /tmp -f /tmp/docker-$VERSION.tgz

# Become "root" for following actions
USER root

# Save the docker client binary
RUN mv /tmp/docker/docker /usr/bin

# cleanup the downloaded files
RUN rm -rf /tmp/docker-$VERSION /tmp/docker

# Install and configure su-exec to enable Jenkins to execute the docker client (which requires root)
# Ex: su-exec root docker images
RUN apk add --no-cache su-exec
RUN chmod u+s /sbin/su-exec

# drop back down to "jenkins" user
USER jenkins