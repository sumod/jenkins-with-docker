﻿Jenkins with Docker
===================
[jenkins/jenkins:lts-alpine][1] based image that enables Jenkins to execute docker client commands on the docker host.  
`docker pull sumod/jenkins-with-docker`

docker build
------------
`docker build -t sumod/jenkins-with-docker .`


docker run
----------
`docker run -p 8080:8080 -p 50000:50000 --mount type=bind,src=/DATA_VOLUMES/jenkins_home,dst=/var/jenkins_home -v //var/run/docker.sock:/var/run/docker.sock --name=docker-jenkins sumod/jenkins-with-docker`

`-p` mappings are for Jenkins services,  
`-mount` maps a host folder to jenkins_home in the docker container enabling Jenkins persistence across container lifecycle  
`-v` maps the docker host sock to the container sock so that docker commands in the container operate on the host (not DinD). `"//"` is required on Windows host, as tested with Docker Toolbox. `"/"` might work on unix hosts.  
`--name` container name for further operations (Ex: look around with: `docker exec -it -u root docker-jenkins bash` once the container is running)  
`sumod/jenkins-with-docker` is the image name tagged in the build step

Jenkins docker command
----------------------
`su-exec root docker images`


[1]: https://hub.docker.com/r/jenkins/jenkins/